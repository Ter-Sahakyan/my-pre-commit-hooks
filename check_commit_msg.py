#!/usr/bin python

from __future__ import annotations

import argparse

import requests
import re
import subprocess


class NeedFixCommitMessages(Exception):
    def __init__(self, commits):
        text = "Wrong commit message(s), please fix :\n"
        for commit in commits:
            text += f"    Commit hash: {commit['hash']} --> Correct message: {commit['msg']}\n"
        super().__init__(text)


class CheckCommitMSG:

    def __init__(self, args: argparse.ArgumentParser):
        self.args = args
        self.separator_symbol = '"' + "'"

    @staticmethod
    def _cmd_output(*cmd: str, **kwargs) -> str:
        kwargs.setdefault('stdout', subprocess.PIPE)
        kwargs.setdefault('stderr', subprocess.PIPE)
        proc = subprocess.Popen(cmd, **kwargs)
        stdout, stderr = proc.communicate()
        stdout = stdout.decode()
        return stdout

    def _git_response_to_list(self, text: str):
        return [
            (x.split('--')[0], '--'.join(x.split('--')[1:])) for x in text.replace('\n', '').split(
                self.separator_symbol
            )[:-1]
        ]

    @staticmethod
    def _post(url, data=None, headers=None):
        try:
            rs = requests.post(url, json=data, headers=headers)
            rs.raise_for_status()
        except requests.exceptions.HTTPError as e:
            raise e
        else:
            return rs.json()

    def _get_access_token(self):
        return self._post(
            f'{self.args.host_url}/api/v1/auth/login/',
            data={'username': self.args.username, 'password': self.args.password}
        )['access']

    def verify_from_server(self, repo_server, branch_name, unpushed_commits) -> list[dict[str]]:
        return self._post(
            f'{self.args.host_url}/api/v1/tasks/check_git_commits',
            data={
                'repo_server': repo_server,
                'branch_name': branch_name,
                'unpushed_commits': unpushed_commits,
            },
            headers={'Authorization': f'JWT {self._get_access_token()}'}
        )

    def execute(self):
        branch_name = self._cmd_output('git', 'branch', '--show-current').replace('\n', '')
        unpushed_commits = self._git_response_to_list(
            self._cmd_output('git', 'log', f'origin/{branch_name}..HEAD', f'--pretty=%H--%s{self.separator_symbol}'),
        )
        if not unpushed_commits:
            return
        repo_server = re.search(r'git@(.*?):', self._cmd_output('git', 'config', '--get', 'remote.origin.url')).group(1)
        fixed_commits = self.verify_from_server(repo_server, branch_name, unpushed_commits)
        if fixed_commits:
            raise NeedFixCommitMessages(fixed_commits)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('filenames', nargs='*')
    parser.add_argument('-u', '--username', required=True)
    parser.add_argument('-p', '--password', required=True)
    parser.add_argument('-hu', '--host_url', required=True)
    check_commit_msg = CheckCommitMSG(parser.parse_args())
    try:
        check_commit_msg.execute()
    except Exception as e:
        raise SystemExit(e)


if __name__ == '__main__':
    main()
